﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ETT.Models
{
    public class RegisterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PassWord { get; set; }
        public string ConfirmPassWord { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }


    }
}