﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ETT.Models
{
    public class ProductModel
    {
        [Required]
        public string Name { get; set; }
        public string Type { get; set; }

    }
}