﻿namespace ETT.Models
{
    public class UploadModel
    {
        public string Name { get; set; }
        public string FileUrl { get; set; }

    }
}