﻿Create procedure [dbo].[InsertLog] 
(
	@level nvarchar(max),
	@callSite nvarchar(max),
	@type nvarchar(max),
	@message nvarchar(max),
	@stackTrace nvarchar(max),
	@innerException nvarchar(max),
	@additionalInfo nvarchar(max)
)
as

insert into dbo.Logs
(
	[Level],
	CallSite,
	[Type],
	[Message],
	StackTrace,
	InnerException,
	AdditionalInfo
)
values
(
	@level,
	@callSite,
	@type,
	@message,
	@stackTrace,
	@innerException,
	@additionalInfo
)

go