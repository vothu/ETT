﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ETT.Helper;
using ETT.IService;
using ETT.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Swashbuckle.Swagger.Annotations;


namespace ETT.Controllers
{
    /// <summary>
    /// Azure
    /// </summary>
    [RoutePrefix("api/v1/azureblob")]
    public class AzureBlobController : BaseApiController
    {
        private readonly IAzureService _azureService;
        private readonly ICustomerService _customerService;

        public AzureBlobController(IAzureService azureService, ICustomerService customerService)
        {
            _azureService = azureService;
            _customerService = customerService;
        }

        [HttpPost]
        [Route("upload")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> UploadImage()
        {

            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }
            var request = Request.Content;
            var data = await request.ReadAsMultipartAsync();
            using (var file = data.Contents[0])
            {
                if (file.Headers.ContentLength > 0 && file.Headers.ContentType.ToString().Contains("image"))
                {
                    var fileData = await file.ReadAsByteArrayAsync();
                    try
                    {
                        var fileBlobContainer = AzureHelper.ImagesBlobContainer;
                        var fileName = Guid.NewGuid().ToString();
                        var url = _azureService.SavingFileToAzureBlob(fileData, fileName,
                            file.Headers.ContentType.ToString(), fileBlobContainer);
                        var resopnse = new UploadModel
                        {
                            Name = url,
                            FileUrl =
                                $"{ConfigurationManager.AppSettings["AzureStorageUrl"]}/{ConfigurationManager.AppSettings["File"]}/{url}"
                        };
                        return SuccessResult(resopnse, "Upload succesfully");
                    }
                    catch (Exception ex)
                    {
                        return ErrorResult(ex.ToString());
                    }
                }
                file.Dispose();
            }
            return ErrorResult("File upload does not support.");

        }


        [HttpGet]
        [Route("download/{filename}/{url}")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> DownloadImage(string filename, string url)
        {

            var memoryStream = new MemoryStream();
            //download files
            using (var zip = new ZipArchive(memoryStream, ZipArchiveMode.Create))
            {
                ZipArchiveEntry imageZip = zip.CreateEntry(filename);
                using (var imageStream = imageZip.Open())
                {
                    //    var blockBlob =
                    //        AzureHelper.ImagesBlobContainer.GetBlockBlobReference(
                    //            image.AzureBlobUrl.Substring(image.AzureBlobUrl.IndexOf("/") + 1));var blockBlob =
                    var blockBlob = AzureHelper.ImagesBlobContainer.GetBlockBlobReference(url);
                    if (blockBlob.Exists())
                    {
                        var fileByteLength = blockBlob.Properties.Length;
                        var bytes = new byte[fileByteLength];
                        await blockBlob.DownloadToByteArrayAsync(bytes, 0);
                        imageStream.Write(bytes, 0, bytes.Length);
                    }
                    else
                    {
                        Trace.WriteLine("not exist file");
                    }
                }

            }
            return SuccessResult("Download Successfully");
        }

        [HttpPost]
        [Route("export")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Export()
        {

            var stream = new MemoryStream();
            var pageSize = PageSize.A4;
            var doc = new Document(pageSize);
            PdfWriter.GetInstance(doc, stream);
            doc.Open();

            var tableHeader = new PdfPTable(4);
            tableHeader.AddCell(new PdfPCell(new Phrase("ID"))
            {
                Border = 2,
                BackgroundColor = BaseColor.GRAY,
                HorizontalAlignment = Element.ALIGN_CENTER,
                Chunks = { new Chunk("abc") }
            });
            tableHeader.AddCell(new PdfPCell(new Phrase("FirstName"))
            {
                Border = 2,
                BackgroundColor = BaseColor.GRAY,
                HorizontalAlignment = Element.ALIGN_CENTER
            });
            tableHeader.AddCell(new PdfPCell(new Phrase("LastName"))
            {
                Border = 2,
                BackgroundColor = BaseColor.GRAY,
                HorizontalAlignment = Element.ALIGN_CENTER
            });
            tableHeader.AddCell(new PdfPCell(new Phrase("Bỉthday"))
            {
                Border = 2,
                BackgroundColor = BaseColor.GRAY,
                HorizontalAlignment = Element.ALIGN_CENTER
            });
            doc.Add(tableHeader);
            var table = new PdfPTable(4);
            var cus = await _customerService.GetAllAsync();
            foreach (var item in cus)
            {
                table.AddCell(new PdfPCell(new Phrase(item.Id.ToString()))
                {
                    Border = 2,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase(item.FirstName))
                {
                    Border = 2,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase(item.LastName))
                {
                    Border = 2,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });

                table.AddCell(new PdfPCell(new Phrase(item.Birthday.ToString()))
                {
                    Border = 2,
                    BackgroundColor = BaseColor.LIGHT_GRAY,
                    HorizontalAlignment = Element.ALIGN_CENTER,
                });
            }
            
            doc.Add(table);
            doc.Close();
            byte[] file = null;
            file =stream.ToArray();
            string mediaType = "application/pdf";

            return new FileActionResult(new MemoryStream(file), mediaType,
                "Orders_" + DateTime.UtcNow.ToString("MM-dd-yyyy") + "." + "pdf");
        }
    }
}

