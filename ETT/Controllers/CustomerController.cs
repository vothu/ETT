﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using ETT.Enties;
using ETT.IService;
using ETT.Models;
using NLog;
using Swashbuckle.Swagger.Annotations;

namespace ETT.Controllers
{
    /// <summary>
    /// CustomerController
    /// </summary>
    [RoutePrefix("api/v1/customer")]
    public class CustomerController : BaseApiController
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Create(CustomerModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var cus = new Customer
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Birthday = model.Birthday
                };
                await _customerService.CreateAsync(cus);
                var result = new CustomerModel
                {
                    FirstName = cus.FirstName,
                    LastName = cus.LastName,
                    Birthday = cus.Birthday
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                Logger logger = LogManager.GetLogger("databaseLogger");
                logger.Error(ex, "Whoops!");
                throw;
            }
            
        }

        /// <summary>
        /// GetCusTomer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> GetById(int id)
        {
            var cus = await _customerService.GetByIdAsync(id);
            if(cus == null) return Json("Error");
            var result = new CustomerModel
            {
                FirstName = cus.FirstName,
                LastName = cus.LastName,
                Birthday = cus.Birthday
            };
            return Json(result);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("{id}")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Update([FromUri] int id ,[FromBody]CustomerModel model)
        {
            if (!ModelState.IsValid)
            {
                foreach (var key in ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0))
                {
                    return Json(ModelState[key].Errors[0].ErrorMessage);
                }
            }
            var cus = await _customerService.GetByIdAsync(id);
            if (cus == null) return Json("Error");
            cus.LastName = model.LastName;
            cus.FirstName = model.FirstName;
            cus.Birthday = model.Birthday;
            await _customerService.UpdateAsync(cus);
            return Json("Successfully");
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Delete([FromUri] int id)
        {

            var cus = await _customerService.GetByIdAsync(id);
            if (cus == null) return Json("Error");
            await _customerService.DeleteAsync(cus);
            return Json("Successfully");
        }

    }
}
