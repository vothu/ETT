﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace ETT.Controllers
{
    public class BaseApiController : ApiController
    {
       
        /// <summary>
        /// prepare error result
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        [NonAction]
        public IHttpActionResult ErrorResult(string errorMessage)
        {
            var errorMessages = new List<string> { errorMessage };
            var dataResult = new
            {
                Status = false, 
                ErrorMessages = errorMessages
            };

            var json = new JavaScriptSerializer().Serialize(dataResult);
            return Json(json);
        }

        /// <summary>
        /// prepare success result
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        public IHttpActionResult SuccessResult(string message)
        {
            var dataResult = new
            {
                Status = true,
                Message = message
            };

            var json = new JavaScriptSerializer().Serialize(dataResult);
            return Json(json);
        }

        /// <summary>
        /// prepare success result
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        public IHttpActionResult SuccessResult(object obj, string message)
        {
            var dataResult = new
            {
                Status = true,
                Message = message,
                Data = obj
            };

            var json = new JavaScriptSerializer().Serialize(dataResult);
            return Json(json);
        }
    }
}