﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace ETT.Controllers
{
    public class FileActionResult : IHttpActionResult
    {
        /// <summary>
        /// FileActionResult
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="mediaType"></param>
        /// <param name="fileName"></param>
        public FileActionResult(Stream stream, string mediaType, string fileName)
        {
            this.Stream = stream;
            this.MediaType = mediaType;
            this.FileName = fileName;
        }
        /// <summary>
        /// Stream of file
        /// </summary>
        public Stream Stream { get; private set; }

        /// <summary>
        /// Media type e.g application/pdf or application/xlsx
        /// </summary>
        public string MediaType { get; set; }

        /// <summary>
        /// file name i.e order1.pdf
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// ExecuteAsync
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StreamContent(Stream);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = FileName
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaType);
            response.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");

            return Task.FromResult(response);
        }
    }
}