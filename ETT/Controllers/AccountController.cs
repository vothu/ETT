﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using ETT.IService;
using ETT.Models;
using Swashbuckle.Swagger.Annotations;

namespace ETT.Controllers
{
    [RoutePrefix("api/v1/account")]
    public class AccountController : BaseApiController
    {

        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        [SwaggerResponse(200, "Returns the result of register new account")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Register(RegisterModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    foreach (var key in ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0))
                    {
                        return ErrorResult(ModelState[key].Errors[0].ErrorMessage);
                    }
                }

                var user = await _userService.GetByUserNameAndEmail(model.UserName, model.Email);
                if(user == null) return ErrorResult("User is exist");


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return Ok();
        }

    }
}
