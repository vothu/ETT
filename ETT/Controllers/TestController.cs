﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OfficeOpenXml;

namespace ETT.Controllers
{
    [RoutePrefix("api/v1/test")]
    public class TestController : BaseApiController
    {


        [Route("")]
        [HttpPost]
        [SwaggerResponse(200, "Returns the result of register new account")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> ChangeBinaryFile()
        {
            // This endpoint only supports multipart form data
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }
            var request = Request.Content;
            var data = await request.ReadAsMultipartAsync();
            using (var file = data.Contents[0])
            {
                if (file.Headers.ContentLength > 0)
                {
                    var fileData = await file.ReadAsByteArrayAsync();
                    var result = string.Concat(fileData.Select(b => Convert.ToString(b, 2)));

                    var stream = new MemoryStream();
                    var xlPackage = new ExcelPackage(stream);
                    var wordWorksheet = xlPackage.Workbook.Worksheets.Add("Sheet1");
                    int j = 1;
                    foreach (var i in result)
                    {
                        wordWorksheet.Cells[j, 10].Value = i.ToString();
                        j++;
                    }



                    OpenFileDialog openDlg = new OpenFileDialog();
                    openDlg.InitialDirectory = @"C:\";
                    openDlg.ShowDialog();
                    string path = openDlg.FileName;

                    if (openDlg.ShowDialog() == DialogResult.OK)
                    {

                        FileInfo fileInfo = new FileInfo(openDlg.CheckPathExists.ToString());
                        xlPackage.SaveAs(fileInfo);
                    }
                }
            }
            return ErrorResult("Convert fail");
        }
    }
}
