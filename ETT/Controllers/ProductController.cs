﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using ETT.Enties;
using ETT.Generic;
using ETT.Models;
using Swashbuckle.Swagger.Annotations;

namespace ETT.Controllers
{
    /// <summary>
    /// ProductController
    /// </summary>
    [RoutePrefix("api/v1/product")]
    public class ProductController : BaseApiController
    {
        readonly UnitOfWork _unitOfWork;
        public ProductController()
        {
            _unitOfWork = new UnitOfWork();
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Get()
        {
            var pro = _unitOfWork.ProductRepository.Get(includeProperties: "Department");
            var list = pro.Select(m => new ProductModel
            {
                Name = m.Name,
                Type = m.Type
            });
            return SuccessResult(list, "Get Successfully");
        }


        /// <summary>
        /// Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Details(int id)
        {
            var product = _unitOfWork.ProductRepository.GetById(id);
            var result = new ProductModel
            {
                Name = product.Name,
                Type = product.Type
            };
            return SuccessResult(result, "Get Successfully");
        }


        /// <summary>
        /// Create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Create(ProductModel model)
        {

            if (!ModelState.IsValid)
            {
                foreach (var key in ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0))
                {
                    return Json(ModelState[key].Errors[0].ErrorMessage);
                }
            }

            var product = new Product
            {
                Name = model.Name,
                Type = model.Type
            };
            _unitOfWork.ProductRepository.Insert(product);
            _unitOfWork.Save();

            return SuccessResult("Create Successfully");

        }



        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Edit(ProductModel model)
        {
            if (!ModelState.IsValid)
            {
                foreach (var key in ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0))
                {
                    return Json(ModelState[key].Errors[0].ErrorMessage);
                }
            }

            var product = new Product
            {
                Name = model.Name,
                Type = model.Type
            };
            _unitOfWork.ProductRepository.Update(product);
            _unitOfWork.Save();
            return SuccessResult("Edit Successfully");

        }


        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("")]
        [SwaggerResponse(200, "")]
        [SwaggerResponse(401, "Don't have permission")]
        [SwaggerResponse(500, "Internal Server Error")]
        [SwaggerResponse(400, "Bad Request")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            _unitOfWork.ProductRepository.Delete(id);
            _unitOfWork.Save();
            return SuccessResult("Delete Successfully");

        }
    }
}
