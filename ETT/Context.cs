﻿using System.Data.Entity;
using ETT.Enties;
using ETT.Enties.Authentication;
using ETT.Mapping;
using ETT.Mapping.Authentication;

namespace ETT
{
    public class Context : DbContext
    {
        public Context() : base("ETT")
        {

        }

        public Context(string connectionStrings) : base(connectionStrings)
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CustomerMapping());
            modelBuilder.Configurations.Add(new OrderDetailMapping());
            modelBuilder.Configurations.Add(new OrderMapping());
            modelBuilder.Configurations.Add(new ProductMapping());
            modelBuilder.Configurations.Add(new UserRoleMapping());
            modelBuilder.Configurations.Add(new UserLoginMapping());
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserClaim> UserClaim { get; set; }
        public DbSet<Role> Role { get; set; }

       
    }

    public class NoisContextInitializer : CreateDatabaseIfNotExists<Context>
    {
        protected override void Seed(Context context)
        {

        }

    }
}