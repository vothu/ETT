﻿using System;
using System.Data.Entity;
using ETT.Enties;

namespace ETT.Generic
{
    public class UnitOfWork : IDisposable
    {
        private readonly Context _context;
        private GenericRepository<Customer> _customeRepository;
        private GenericRepository<Order> _orderRepository;
        private GenericRepository<OrderDetail> _orderDetailRepository;
        private GenericRepository<Product> _productRepository;

        public UnitOfWork()
        {
            _context = new Context("ETT");
        }
      
        public GenericRepository<Customer> CustomerRepository
        {
            get
            {
                if (_customeRepository == null)
                {
                    _customeRepository = new GenericRepository<Customer>(_context);
                }
                return _customeRepository;
            }
        }
       
        public GenericRepository<Product> ProductRepository
        {
            get
            {

                if (_productRepository == null)
                {
                    _productRepository = new GenericRepository<Product>(_context);
                }
                return _productRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed = false ;


       

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}