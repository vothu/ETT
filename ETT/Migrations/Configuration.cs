using System.Collections.Generic;
using ETT.Enties.Authentication;
using ETT.Mapping;

namespace ETT.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ETT.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

     

        protected override void Seed(ETT.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            ////create system role
            //var roles = CreateSystemRole();
            //var roleSet = context.Set<Role>();
            //foreach (var item in roles)
            //{
            //    roleSet.AddOrUpdate(p => p.Name, item);
            //}
        }

        private List<Role> CreateSystemRole()
        {
            return new List<Role>
            {
                new Role {Id=1, Name = RoleSystemName.Administrator, NormalizeName = "Administrator"},
                new Role {Id=2, Name = RoleSystemName.Accounting, NormalizeName = "Accounting"},
            };

        }
    }
}
