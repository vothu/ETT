namespace ETT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeAllowNullLogDatetime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Log", "LoggedOnDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Log", "LoggedOnDate", c => c.DateTime(nullable: false));
        }
    }
}
