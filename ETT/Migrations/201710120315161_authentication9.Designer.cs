// <auto-generated />
namespace ETT.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class authentication9 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(authentication9));
        
        string IMigrationMetadata.Id
        {
            get { return "201710120315161_authentication9"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
