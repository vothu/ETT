namespace ETT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLog2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Log",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Level = c.String(),
                        CallSite = c.String(),
                        Type = c.String(),
                        Message = c.String(),
                        StackTrace = c.String(),
                        InnerException = c.String(),
                        AdditionalInfo = c.String(),
                        LoggedOnDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Log", new[] { "Id" });
            DropTable("dbo.Log");
        }
    }
}
