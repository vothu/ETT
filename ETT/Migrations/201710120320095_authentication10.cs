namespace ETT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class authentication10 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Users", "ApplicationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "ApplicationId", c => c.Int(nullable: false));
        }
    }
}
