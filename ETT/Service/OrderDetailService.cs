﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ETT.Enties;
using ETT.IService;

namespace ETT.Service
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly DbContext _context;
        private readonly DbSet<OrderDetail> _dbSet;

        public OrderDetailService(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<OrderDetail>();
        }
        public Task<OrderDetail> GetByIdAsync(int id)
        {
            try
            {
                var query = "Select * from [OrderDetail] where id = @p0";
                var res = _dbSet.SqlQuery(query, id).FirstOrDefault();
                return Task.FromResult(res);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }

        }

        public async Task CreateAsync(OrderDetail entity)
        {
            try
            {
                _dbSet.Add(entity);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task UpdateAsync(OrderDetail entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task DeleteAsync(OrderDetail entity)
        {
            try
            {
                await _context.Database.ExecuteSqlCommandAsync("Delete [OrderDetail] where Id = @p0", entity.Id);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }
    }
}