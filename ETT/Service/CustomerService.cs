﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ETT.Enties;
using ETT.IService;

namespace ETT.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly DbContext _context;
        private readonly DbSet<Customer> _dbSet;

        public CustomerService(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<Customer>();
        }
        public Task<Customer> GetByIdAsync(int id)
        {
            try
            {
                var query = "Select * from [Customer] where id = @p0";
                var res = _dbSet.SqlQuery(query, id).FirstOrDefault();
                return Task.FromResult(res);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }

        }

        public async Task CreateAsync(Customer entity)
        {
            try
            {
                _dbSet.Add(entity);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task UpdateAsync(Customer entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task DeleteAsync(Customer entity)
        {
            try
            {
                await _context.Database.ExecuteSqlCommandAsync("Delete [Customer] where Id = @p0", entity.Id);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public  Task<List<Customer>> GetAllAsync()
        {
            try
            {
                var query = "Select * from [Customer]";
                return _dbSet.SqlQuery(query).ToListAsync();

            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }
    }
}