﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ETT.Enties;
using ETT.Enties.Authentication;
using ETT.IService;

namespace ETT.Service
{
    public class UserService : IUserService
    {
        private readonly DbContext _context;
        private readonly DbSet<User> _dbSet;

        public UserService(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<User>();
        }
        public Task<User> GetByIdAsync(int id)
        {
            try
            {
                var query = "Select * from [User] where id = @p0";
                var res = _dbSet.SqlQuery(query, id).FirstOrDefault();
                return Task.FromResult(res);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }

        }

        public async Task CreateAsync(User entity)
        {
            try
            {
                _dbSet.Add(entity);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task UpdateAsync(User entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task DeleteAsync(User entity)
        {
            try
            {
                await _context.Database.ExecuteSqlCommandAsync("Delete [User] where Id = @p0", entity.Id);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public  Task<User> GetByUserNameAndEmail(string username, string email)
        {
            try
            {
                var query = "select * from User where UserName = @p0 and Email = @p1";
                return _dbSet.SqlQuery(query, username, email).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}