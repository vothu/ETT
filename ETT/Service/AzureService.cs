﻿using ETT.IService;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ETT.Service
{
    public class AzureService : IAzureService
    {
        public string SavingFileToAzureBlob(byte[] bytes,string name, string contentType, CloudBlobContainer cloudBlobContainer)
        {
            var container = cloudBlobContainer;
            container.CreateIfNotExists();
            container.SetPermissions(
                new BlobContainerPermissions
                {
                    PublicAccess =
                        BlobContainerPublicAccessType.Blob
                });


            var blob = container.GetBlockBlobReference(name);
            blob.Properties.ContentType = contentType;
            blob.UploadFromByteArray(bytes,0,bytes.Length);
            return name + "." + contentType.Split('/')[1];
        }
    }
}