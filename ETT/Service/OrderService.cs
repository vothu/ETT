﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ETT.Enties;
using ETT.IService;

namespace ETT.Service
{
    public class OrderService : IOrderService
    {
        private readonly DbContext _context;
        private readonly DbSet<Order> _dbSet;

        public OrderService(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<Order>();
        }
        public Task<Order> GetByIdAsync(int id)
        {
            try
            {
                var query = "Select * from [Order] where id = @p0";
                var res = _dbSet.SqlQuery(query, id).FirstOrDefault();
                return Task.FromResult(res);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }

        }

        public async Task CreateAsync(Order entity)
        {
            try
            {
                _dbSet.Add(entity);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task UpdateAsync(Order entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task DeleteAsync(Order entity)
        {
            try
            {
                await _context.Database.ExecuteSqlCommandAsync("Delete [Order] where Id = @p0", entity.Id);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }
    }
}