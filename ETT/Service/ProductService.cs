﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ETT.Enties;
using ETT.IService;

namespace ETT.Service
{
    public class ProductService : IProductService
    {
        private readonly DbContext _context;
        private readonly DbSet<Product> _dbSet;

        public ProductService(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<Product>();
        }
        public Task<Product> GetByIdAsync(int id)
        {
            try
            {
                var query = "Select * from [Product] where id = @p0";
                var res = _dbSet.SqlQuery(query, id).FirstOrDefault();
                return Task.FromResult(res);
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }

        }

        public async Task CreateAsync(Product entity)
        {
            try
            {
                _dbSet.Add(entity);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task UpdateAsync(Product entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }

        public async Task DeleteAsync(Product entity)
        {
            try
            {
                await _context.Database.ExecuteSqlCommandAsync("Delete [Product] where Id = @p0", entity.Id);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception: " + ex);
            }
        }
    }
}