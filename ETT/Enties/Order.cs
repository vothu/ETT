﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ETT.Enties
{
    public class Order
    {
        [Key]
        [Required]
        [Index]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public DateTime Created { get; set; }
        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}