﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ETT.Enties.Authentication
{
    public class Role : IdentityRole<int, UserRole>
    {
        [MaxLength(250)]
        public string NormalizeName { get; set; }
    }

    public static class RoleSystemName
    {
        public static string Administrator = "Administrator";
        public static string Accounting = "Accounting";
    }
}