﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ETT.Enties
{
    public class Image 
    {
        /// <summary>
        /// FileName
        /// </summary>
        [Required]
        [MaxLength(500)]
        public string FileName { get; set; }

        /// <summary>
        /// AzureBlobUrl
        /// </summary>
        [Required]
        public string AzureBlobUrl { get; set; }

        /// <summary>
        /// Size
        /// </summary>
        public long? Size { get; set; }

        /// <summary>
        /// LocalPath
        /// </summary>
        public string LocalPath { get; set; }

        /// <summary>
        /// ImageHash
        /// </summary>
        [Required]
        public string ImageHash { get; set; }

        /// <summary>
        /// Azure search auto convert form local to UTC
        /// </summary>
        public DateTime DateUploaded { get; set; }
    }
}