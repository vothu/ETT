﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ETT.Enties;

namespace ETT.IService
{
    public interface ICustomerService
    {
        /// <summary>
        /// GetByIdAsync
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Customer> GetByIdAsync(int id);


        /// <summary>
        /// CreateAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task CreateAsync(Customer entity);

        /// <summary>
        /// UpdateAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task UpdateAsync(Customer entity);

        /// <summary>
        /// DeleteAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task DeleteAsync(Customer entity);

        /// <summary>
        /// GetAllAsync
        /// </summary>
        /// <returns></returns>
        Task<List<Customer>> GetAllAsync();
    }
}
