﻿using Microsoft.WindowsAzure.Storage.Blob;

namespace ETT.IService
{
    public interface IAzureService
    {
        /// <summary>
        /// SavingFileToAzureBlob
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="name"></param>
        /// <param name="contentType"></param>
        /// <param name="cloudBlobContainer"></param>
        /// <returns></returns>
        string SavingFileToAzureBlob(byte[] bytes, string name, string contentType,
            CloudBlobContainer cloudBlobContainer);
    }
}
