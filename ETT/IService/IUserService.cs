﻿using System.Threading.Tasks;
using ETT.Enties;
using ETT.Enties.Authentication;

namespace ETT.IService
{
    public interface IUserService
    {
        /// <summary>
        /// GetByIdAsync
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<User> GetByIdAsync(int id);


        /// <summary>
        /// CreateAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task CreateAsync(User entity);

        /// <summary>
        /// UpdateAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task UpdateAsync(User entity);

        /// <summary>
        /// DeleteAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task DeleteAsync(User entity);

        /// <summary>
        /// GetByUserNameAndEmail
        /// </summary>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        Task<User> GetByUserNameAndEmail(string username, string email);
    }
}
