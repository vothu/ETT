﻿using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ETT.Helper
{
    public class AzureHelper
    {

        private static CloudBlobClient _cloudBlobClient;

        public static CloudBlobClient CloudBlobClient
        {
            get
            {
                if (_cloudBlobClient == null)
                {
                    var blobStorageConnectionString = ConfigurationManager.AppSettings["BlobStorageConnectionString"];
                    // Create blob client and return reference to the container
                    var blobStorageAccount = CloudStorageAccount.Parse(blobStorageConnectionString);
                    _cloudBlobClient = blobStorageAccount.CreateCloudBlobClient();
                }

                return _cloudBlobClient;
            }
        }

        private static CloudBlobContainer _imageBlobContainer;

        public static CloudBlobContainer ImagesBlobContainer
        {
            get
            {
                if (_imageBlobContainer == null)
                {
                    var imagesFolderName = ConfigurationManager.AppSettings["ImageContainerName"];
                    // Retrieve a reference to a container. 
                    _imageBlobContainer = CloudBlobClient.GetContainerReference(imagesFolderName);
                    // Create the container if it doesn't already exist.
                    _imageBlobContainer.CreateIfNotExists();
                    _imageBlobContainer.SetPermissions(
                        new BlobContainerPermissions
                        {
                            PublicAccess =
                                BlobContainerPublicAccessType.Container
                        });
                }
                return _imageBlobContainer;
            }
        }
    }
}