﻿using System.Data.Entity.ModelConfiguration;
using ETT.Enties.Authentication;

namespace ETT.Mapping.Authentication
{
    public class UserRoleMapping : EntityTypeConfiguration<UserRole>
    {
        public UserRoleMapping()
        {
            // Primary Key
            HasKey(t => new { t.UserId, t.RoleId });

            // Table & Column Mappings
            ToTable("UserRole");
        }
    }
}