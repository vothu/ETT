﻿using System.Data.Entity.ModelConfiguration;
using ETT.Enties.Authentication;

namespace ETT.Mapping.Authentication
{
    public class UserLoginMapping : EntityTypeConfiguration<UserLogin>
    {
        public UserLoginMapping()
        {
            HasKey(t => new { t.LoginProvider, t.UserId, t.ProviderKey });
            // Table & Column Mappings
            ToTable("UserLogin");

        }
    }
}