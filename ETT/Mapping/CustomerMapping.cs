﻿using System.Data.Entity.ModelConfiguration;
using ETT.Enties;

namespace ETT.Mapping
{
    public class CustomerMapping : EntityTypeConfiguration<Customer>
    {
        public CustomerMapping()
        {
            HasKey(t => t.Id);
            ToTable("Customer");
        }
    }
}