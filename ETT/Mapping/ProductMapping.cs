﻿using System.Data.Entity.ModelConfiguration;
using ETT.Enties;

namespace ETT.Mapping
{
    public class ProductMapping : EntityTypeConfiguration<Product>
    {
        public ProductMapping()
        {
            HasKey(t => t.Id);
            ToTable("Product");
        }
    }
}