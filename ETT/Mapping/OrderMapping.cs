﻿using System.Data.Entity.ModelConfiguration;
using ETT.Enties;

namespace ETT.Mapping
{
    public class OrderMapping : EntityTypeConfiguration<Order>
    {
        public OrderMapping()
        {
            HasKey(t => t.Id);
            ToTable("Order");
            HasRequired(m => m.Customer).WithMany(m => m.Orders).HasForeignKey(m => m.CustomerId);
        }
    }
}