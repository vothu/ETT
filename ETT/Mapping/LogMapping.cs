﻿using System.Data.Entity.ModelConfiguration;
using ETT.Enties;

namespace ETT.Mapping
{
    public class LogMapping : EntityTypeConfiguration<Log>
    {
        public LogMapping()
        {
            HasKey(t => t.Id);
            ToTable("Log");
        }
    }
}